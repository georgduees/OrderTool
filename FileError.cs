
namespace OrderTool
{
    public class FileError: System.Exception


    {
        public FileError(System.Exception inner) : base(inner.Message, inner) { }
     
        
    }
}