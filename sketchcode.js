
function changeDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  
  function getDateString(date) {
    return ('0' + date.getDate()).slice(-2) + '.'
    + ('0' + (date.getMonth()+1)).slice(-2) + '.'
    + date.getFullYear();
  }
  function drawOrderInfo(curLine){
     console.log(`Order: ${curLine.OrderId} Start: ${getDateString(curLine.Start)} End: ${getDateString(curLine.End)} Price: ${curLine.Price} `);
  }
  
  function outPut(arr) {
    for (var i = 0; i < arr.length; i++) {
      drawOrderInfo(arr[i]);
     
    }
  }
  
  
  /*
  SEITE
  Start; End; Price; OrderId
  01.01.2015; 01.03.2015; 20; 1
  01.02.2015; 31.12.2015; 18; 2
  01.06.2015; 01.09.2015; 16; 3
  */
  var inputArray =[  {
       "OrderId":2,
       "Price":18,
       "Start": new Date(2015, 1, 1),
       "End": new Date(2015, 11, 31) 
    },{
       "OrderId":1,
       "Price":20,
       "Start": new Date(2015, 0, 1),
       "End": new Date(2015, 2, 1) 
    },
   
    {
       "OrderId":3,
       "Price":16,
       "Start": new Date(2015, 5, 1),
       "End": new Date(2015, 8, 1) 
    }
    ];
  
  
  
  //sort Array
  //HTMLOutputElement
  console.log("Input");
  outPut(inputArray)
  console.log("DEBUG INFO");
  
  inputArray.sort(function(a, b) {
    return new Date(a.Start) - new Date(b.Start);}
  );
  
  
  
  inputArray.forEach(function(element,i){
  var element= inputArray[i];
  var prev=(i>0)?inputArray[i-1]:element;
  var cur=element;
  
  if(prev.OrderId!=element.OrderId){
    
  console.log(`current Order : ${element.OrderId}`);
  console.log(`current index : ${i}`);
  console.log(`comparing Dates (element.Start<=prev.End ${getDateString(element.Start)} and ${getDateString(prev.End)}`);
  
  console.log(`colliding timezones Order : ${prev.OrderId}`);
  //StartDate Older or equal than Enddate of previous item
  if(element.Start<=prev.End){
  console.log(`Order : ${element.OrderId} starts before ${prev.OrderId} ends.`);
  //check if enddate of current larger than previous enddate to be split
  var oldEndDate=prev.End;
  
  console.log(`Adjust endtime of Order : ${prev.OrderId} to ${getDateString(changeDays(element.Start,-1))} and remember old Enddate ${getDateString(oldEndDate)}.`);
  inputArray[i-1].End=changeDays(element.Start,-1);
  if(element.End>=oldEndDate){
  console.log(`Order : ${element.OrderId} ends after Order ${prev.OrderId} `);
  console.log('nothing to do.')
  console.log('trim enddate')
  //prev.End=subDays(element.Start,1);
  
  
  }else{
    drawOrderInfo(element);
    
  console.log(`ends before`);
  drawOrderInfo(prev);
  
  console.log(`Need to split Order : ${prev.OrderId}`);
  var splittedOrder ={"OrderId":prev.OrderId,"Start":prev.Start,"End":prev.End,"Price":prev.Price};
  splittedOrder.Start=changeDays(element.End,1);
  
  
   
  var curArray= inputArray;
  console.log(JSON.stringify(curArray));
  splittedOrder.End=new Date(oldEndDate);
  //
  // console.log(index);
  curArray.splice(i+1, 0, splittedOrder);
  // inserts at index 1
  }}
  
  }});
  
  console.log("OUTPUT");
  outPut(inputArray);
  
  
    
  
  
  
  
  
inputArray.forEach(function(element,i){
  var element= inputArray[i];
  var prev=(i>0)?inputArray[i-1]:element;
  var cur=element;
  //StartDate Older or equal than Enddate of previous item
    if(element.Start<=prev.End){
      //check if enddate of current larger than previous enddate to be split
      var oldEndDate=prev.End;
      inputArray[i-1].End=changeDays(element.Start,-1);
      if(element.End<oldEndDate){
        console.log(`Need to split Order : ${prev.OrderId}`);
        var splittedOrder ={"OrderId":prev.OrderId,"Start":prev.Start,"End":prev.End,"Price":prev.Price};
        splittedOrder.Start=changeDays(element.End,1);
        var curArray= inputArray;
        splittedOrder.End=new Date(oldEndDate);
        curArray.splice(i+1, 0, splittedOrder);
      }
    }

}});
     
    
      
    
    
    
    