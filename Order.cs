﻿﻿using System;

namespace OrderTool
{
    public class Order
    {
        /// <summary>
        /// Convertst he Order to a readable string.null 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
        //1.1.2015;31.1.2015;20,00
        return Start.ToShortDateString() + ";" +End.ToShortDateString() + ";" + Price.ToString();
        }
        public Order(string inputString = null)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return;
            }
            var curParameters = inputString.Split(';');
            this.OrderNo = Convert.ToInt32(curParameters[3]);
            this.Price = Convert.ToDouble(curParameters[2]);
            this.End = Convert.ToDateTime(curParameters[1]);
            this.Start = Convert.ToDateTime(curParameters[0]);

        }
        public int OrderNo;
        public double Price;
        public DateTime Start;
        public DateTime End;
    }
}