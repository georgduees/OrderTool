using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

namespace OrderTool
{
    public class CSVFile : IFile
    {
        public const string FileType = ".csv";
        public string FileExtension;
        public CSVFile(string filePath)
        {
            this.FilePath=filePath;
        }

        public string FilePath { get ;set; }
       

        public FileResponse Validate
        {
            get
            {
                try
                {
                    if (this.FilePath.EndsWith(FileType))
                    {
                        return FileResponse.OK;
                    }
                    else
                    {
                        return FileResponse.FORMAT_INVALID;
                    }
                }
                catch (System.Exception ex)
                {
                    return FileResponse.ERROR;
                    throw new FileError(ex);
                }
            }
        }

       
        private bool getFileExtension(string inputFilePath, out string fileExtension){
            string[] splitResponse=inputFilePath.Split(".");
            if(splitResponse.Length>1){
                fileExtension=splitResponse[splitResponse.Length-1];
                return true;
            }
            fileExtension="";
            return false;
        }
        public bool IsValidFile(){
            if(getFileExtension(this.FilePath,out this.FileExtension)){
               return FileType.Equals(this.FileExtension);
            }
            return false;
        }

        private Order getOrderFromString(string inputString){
            Order responseOrder = new Order();
            string[] splitResponse= inputString.Split(";");
             responseOrder.OrderNo = Convert.ToInt32(splitResponse[3]);
            responseOrder.Price = Convert.ToDouble(splitResponse[2]);
            responseOrder.End = Convert.ToDateTime(splitResponse[1]);
            responseOrder.Start = Convert.ToDateTime(splitResponse[0]);
            return responseOrder;
        }
        public List<Order> ReadOrderList()
        {
            List<Order> returnList = new List<Order>();
            List<string> stringList = File.ReadLines(this.FilePath).ToList();
            string firstLine =stringList.FirstOrDefault();
            bool hasDefaultHeader=!firstLine.Any(c=>char.IsDigit(c));
            if(hasDefaultHeader){
                stringList.RemoveAt(0);
            }
            for (int i = 0; i < stringList.Count; i++)
            {
                string element = stringList[i];
                try{
                   Order currentOrder = getOrderFromString(element);
                    returnList.Add(currentOrder);

                }catch(Exception ex){
                 Console.WriteLine("Data in File invalid,could not process line:");
                 Console.WriteLine(element);
                 throw ex;

                }

            }
           
            return returnList;
       }
        public FileResponse WriteOrderList(List<Order> orderList,string filePath=null)
        {
            if(filePath==null){
                filePath  = this.FilePath.Insert(this.FilePath.LastIndexOf("."),"_out");
            }
            string [] outputOrderList = new string[orderList.Count];
            for (int i = 0; i < orderList.Count; i++)
            {
                            outputOrderList[i]=orderList[i].ToString();

            }
           
            try{
                File.WriteAllLines(filePath,outputOrderList);
            }catch(Exception ex){
            throw ex;
            
            }
            return FileResponse.ERROR;
        }
         public FileResponse WriteOrderList(List<Order> oList)
        {
            
             string   outfilePath  = this.FilePath.Insert(this.FilePath.LastIndexOf("."),"_out");

            
            
            return       this.WriteOrderList(oList, outfilePath);

        }
    }
}