namespace OrderTool
{
    public enum FileResponse
    {
        ERROR = 0,
        OK = 1,
        FILE_EXISTS = 2,
        FORMAT_INVALID = 4
    }
}