﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderTool
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length==0){
                Console.WriteLine("No command line argument found.");
                Console.WriteLine("Use -h for help.");
                Environment.Exit(0);
            }
            Properties prop = new Properties(args);
            if(prop.InputFile.Length==0){
                Console.WriteLine(" No inputfile selected");
                prop.ShowHelp();
                System.Environment.Exit(0);
            }
            
            //initialize file Open file
            CSVFile file = new CSVFile(prop.InputFile);
           
            try{

            
            switch (file.Validate)
            {
                default:
                case FileResponse.ERROR:
                    Console.WriteLine("A General Error occured.");
                    prop.ShowHelp();
                break;
                case FileResponse.FILE_EXISTS:
                    Console.WriteLine("output-file already exists.");
                break;
                case FileResponse.FORMAT_INVALID:
                    Console.WriteLine("Fileformat wrong.");
                break;
                case FileResponse.OK:
                    if(file.IsValidFile()){
                        try{
                            List<Order> orderList =file.ReadOrderList();
                            orderList=orderList.OrderBy(o=>o.Start).ToList();
                            FilterOrders(orderList);
                            file.WriteOrderList(orderList);
                        }catch(Exception ex){
                            Console.WriteLine("Exception occured: ");
                            Console.WriteLine(ex.Message);
                        }
                    }
                break;
            }
            
            }catch(Exception ex){
                    Console.WriteLine("Exception: {0}",ex.Message);
                    Console.WriteLine("A General Error occured.");
                    prop.ShowHelp();
                    System.Environment.Exit(0);
            }
            
        }

        /// <summary>
        /// Filters the Orders so duplicated/overlapping dateranges are removed.
        /// </summary>
        /// <param name="orders">List of Orderitems to be processed</param>
        private static void FilterOrders(List<Order> orders)
        {
            for (int i = 0; i < orders.Count; i++)
            {
                Order current = (Order)orders[i];
                Order previous=(i>0)?orders[i-1]:current;

                if(current.Start<=previous.End){
                    //check if enddate of current larger than previous enddate to be split
                    DateTime pEnd=previous.End;

                    if(previous!=current){
                        orders[i-1].End=current.Start.AddDays(-1);
                    }

                    if(current.End<pEnd){
                        Order partialOrder = new Order();
                        partialOrder.Start=current.End.AddDays(1);
                        partialOrder.End=pEnd;
                        partialOrder.Price= previous.Price;
                        orders.Insert(i+1,partialOrder);
                    }
                }
            }    

        }
    }
}
