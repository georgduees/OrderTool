﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace OrderTool
{
    public class Properties
    {
        public string CWD;
        public string OutputFile;
        public string InputFile;
        public bool Help;
        public bool Verbose;
        /// <summary>
        /// Shows Help CMD arguments
        /// </summary>
        public void ShowHelp(){
            Console.WriteLine("OrderTool.exe Help:");
            Console.WriteLine("Example: ");
            Console.WriteLine("OrderTool.exe -h shows Help");
            Console.WriteLine("Parameters:");
            Console.WriteLine("' -h' or ' -?'");
            Console.WriteLine("-> Show  Help");
            Console.WriteLine("' -v' or ' -debug'");
            Console.WriteLine("-> Show Verbose output");
            Console.WriteLine("' -f=<FILENAME>'");
            Console.WriteLine("-> defines the inputfile");
         
            Console.WriteLine("All Parameter Keys are case insensitive.");
            Console.WriteLine("If not specified, the Output file has the Name of the Input file with ");
        }
        public Properties(string[] arguments)
        {


        Dictionary<string, string> StartupParameters = new Dictionary<string, string>();
        foreach (var argument in arguments)
        {
            string  filteredArgument = (argument.StartsWith("-"))
                                        ? argument.Substring(1)
                                        : argument;
            string[] argumentList = filteredArgument.Split('=');

            if (argumentList.Length > 1)
            {
                StartupParameters.Add(argumentList[0].ToLower(),
                argumentList[1]);
            }
            else
            {
                StartupParameters.Add(argumentList[0].ToLower(),
                null);
            }

        }
        this.Help = StartupParameters.ContainsKey("h")||StartupParameters.ContainsKey("?");
        this.Verbose = StartupParameters.ContainsKey("v") || StartupParameters.ContainsKey("debug");

        if (this.Help)
        {
            ShowHelp();
        }
        if (this.Verbose)
        {
            Console.WriteLine("Verbose Output {0}",this.Verbose);
        }
        bool parseResponse=StartupParameters.TryGetValue("f",out this.InputFile);
        if (parseResponse && this.Verbose)
        {
            Console.WriteLine("Filename: {0}", this.InputFile);
            parseResponse = false;
        }
            parseResponse = StartupParameters.TryGetValue("o", out this.OutputFile);
        }
    }
}