﻿using System.Collections.Generic;
using System.Xml.Schema;

namespace OrderTool
{
   
    public interface IFile
    {

        /// <summary>
        /// Validates the File if it is a valid Fileformat.
        /// </summary>
        /// <value></value>
        FileResponse Validate { get; }
        /// <summary>
        /// Filepath of the file we want to "sort/filter"
        /// </summary>
        /// <value></value>
        string FilePath{set;get;}
        /// <summary>
        /// Read the Filecontent into a List of Order-objects.
        /// </summary>
        /// <returns></returns>
        List<Order> ReadOrderList();
        /// <summary>
        /// Write a Orderlist to the defualt output-file
        /// </summary>
        /// <param name="orderList"></param>
        /// <returns></returns>
        FileResponse WriteOrderList(List<Order> orderList);
        /// <summary>
        /// Write the Orderlist to the specified Outputfile
        /// </summary>
        /// <param name="orderList">List of order-objects</param>
        /// <param name="filePath">Filename/Path of the Target/output-file.</param>
        /// <returns></returns>
        FileResponse WriteOrderList(List<Order> orderList, string filePath);
    }

  
}