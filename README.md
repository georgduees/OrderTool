# OrderTool
  <h3 align="center">OrderTool</h3>

  <p align="center">
    Sorting of a  Pricelist
    <br />
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

- [OrderTool](#OrderTool)
  - [About The Project](#about-the-project)
    - [Built With](#built-with)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
  - [Usage](#usage)
  - [Contributing](#contributing)
  - [License](#license)
  - [Contact](#contact)
  - [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

This tool sorts entries in CSV Files, it is extensible to be used with other File-Formats as well, but those are not yet implemented.

### Built With

* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [.NET Core](https://dotnet.microsoft.com/download/dotnet-core)
* [Visual Studio Code](https://code.visualstudio.com)

#### Versions used:

.NET Core 2.2

<!-- GETTING STARTED -->
## Getting Started

This Project takes an List of Orders as Input and sorts them so a list of Orders is generated without duplicate date-ranges.

### Download the Project

Clone the Github Repository
```
git clone https://github.com/georgduees/OrderTool.git
```
Change the directory to the repository
```
cd OrderTool
```
Build the project and its dependencies using Release configuration:
```
dotnet build --configuration Release
```
### Download prebuild binaries

1. The Executable can be found in the folder /win10-x64
2. The DLL-Library can be found in the folder /win10-x64 as well.

The tool can be build using the following prerequisites.

### Prerequisites

You need at least a Computer with .NetCore Framework installed, this can either be a Linux/MacOS or Windows Box.
To run the tool you could either open the tool in the same folder, or use it by running 
``` bash
dotnet ./Ordertool.dll
```

### Installation

``` json
//LAUNCH.json
{
   // Use IntelliSense to find out which attributes exist for C# debugging
   // Use hover for the description of the existing attributes
   // For further information visit https://github.com/OmniSharp/omnisharp-vscode/blob/master/debugger-launchjson.md
   "version": "0.2.0",
   "configurations": [
        {
            "name": ".NET Core Launch (console)",
            "type": "coreclr",
            "request": "launch",
            "preLaunchTask": "build",
            // If you have changed target frameworks, make sure to update the program path.
            "program": "${workspaceFolder}/bin/Debug/netcoreapp2.2/OrderTool.dll",
            "args": [ "-v -f=ExampleData.csv"],
            "cwd": "${workspaceFolder}",
            // For more information about the 'console' field, see https://aka.ms/VSCode-CS-LaunchJson-Console
            "console": "internalConsole",
            "stopAtEntry": false
        },
        {
            "name": ".NET Core Attach",
            "type": "coreclr",
            "request": "attach",
            "processId": "${command:pickProcess}"
        }
    ]
}
```


<!-- USAGE EXAMPLES -->
## Usage

The following parameters are available:
OrderTool.exe Help:
Example: 
```sh
$ OrderTool.exe -h 
```
Parameters:

__-h__ or  __-?__

> Show  Help

 __-v__ or  __-debug__

> Show Verbose output

 __-f=\<FILENAME>__

> defines the inputfile

 __-o=\<OUTPUTFILENAME>__

>OPTIONAL:  defines the output filename to be used.

 __-d=\<DirectoryPath>__

>OPTIONAL:  defines the Directory of the File used.
>
>All Parameter Keys are case insensitive.
>
>If not specified, the Output file has the Name of the Input file with 


## Example Data 

The schema for generating example data to parse has been generated at Mockaroo as slim Testdata Generator.

Share Link: https://www.mockaroo.com/292bb670

If you want to generate Data, you can use the following command:

Generate data using curl with the following command:
```bash
curl "https://api.mockaroo.com/api/292bb670?count=50&key=401d0060" > "OrderTool.csv"
```

### Input Data:
```CSV
Start;Ende;Preis;Reihenfolge
01.01.2015;01.03.2015;20;1
01.02.2015;31.12.2015;18;2
01.06.2015;01.09.2015;16;3
```

### Output Data:
```CSV
Start;Ende;Preis
1.1.2015;31.1.2015;20,00
1.2.2015;31.5.2015;18,00
1.6.2015;1.9.2015;16,00
2.9.2015;31.12.2015;18,00
```
<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

Georg Dües - [@georg_duees](https://twitter.com/georg_duees) - georg@duees.de

Project  Link: [https://github.com/georgduees/OrderTool](https://github.com/georgduees/OrderTool)


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Mockaroo.com Testdata-Generator](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Choose an Open Source License](https://choosealicense.com)
* [Timeglass Git Timetracking](https://github.com/timeglass/glass)

## Timeline

-Project Creation 28.05.19
1,5h README initial setup

##ToDo

1. Setup Basic Project [ x ] 
2. Add FileReader FileWriter logic [ x ] 
3. Add Sorting Logic [ x ] 
4. Output to File [ x ] 




## Versioning


[![Version 0.1.0.alpha](https://img.shields.io/badge/Version-0.1.0.alpha-yellow.svg)](https://github.com/georgduees/OrderTool)

For Versioning we use Semantic Versioning  
* [Semantic Versioning 2.0.0](https://semver.org/)
